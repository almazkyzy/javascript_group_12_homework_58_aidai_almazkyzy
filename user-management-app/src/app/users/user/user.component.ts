import { Component, Input, OnInit } from '@angular/core';
import { User } from '../../../shared/user.model';
import { GroupService } from '../../../shared/group.service';



@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  @Input() user!:User;

  constructor(private groupService: GroupService) { }

  ngOnInit(): void {
  }

  onClick() {
    this.groupService.addGroup(this.user);
  }
}
