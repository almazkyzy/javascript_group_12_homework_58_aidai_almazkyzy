import { Component, Input, OnInit } from '@angular/core';
import { Group } from '../../../shared/group.model';
import { GroupService } from '../../../shared/group.service';

@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.css']
})
export class GroupComponent implements OnInit {
  @Input() group!:Group;
  isActive: boolean = false;
  groups!:Group[];

  constructor(public groupService: GroupService) { }

  ngOnInit() {
  }

  onClick() {
    this.isActive = true;
  }
}
