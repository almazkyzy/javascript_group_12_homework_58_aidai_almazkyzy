import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { User } from '../../shared/user.model';
import { UserService } from '../../shared/user.service';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css'],
})

export class NewUserComponent implements OnInit {
  @ViewChild('nameInput') nameInput!: ElementRef;
  @ViewChild('emailInput') emailInput!: ElementRef;
  status: string = '';
  role: string= '';

  constructor(private userService: UserService) {
  }

  addUser(){
    const name = this.nameInput.nativeElement.value;
    const email =this.emailInput.nativeElement.value;
    const status= this.status;
    const role = this.role;

    const userCreated = new User(name, email,status, role);
    this.userService.addUser( userCreated);
  }

  ngOnInit(): void {
  }

}
