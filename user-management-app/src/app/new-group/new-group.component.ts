import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Group } from '../../shared/group.model';
import { GroupService } from '../../shared/group.service';

@Component({
  selector: 'app-new-group',
  templateUrl: './new-group.component.html',
  styleUrls: ['./new-group.component.css']
})
export class NewGroupComponent implements OnInit {
  @ViewChild('nameInput') nameInput!: ElementRef;

  addGroup(){
    const name = this.nameInput.nativeElement.value;
    const groupCreated = new Group(name);
    this.groupService.addGroup(groupCreated);
  }

  constructor(private groupService:GroupService) {}

  ngOnInit(): void {
  }

}
