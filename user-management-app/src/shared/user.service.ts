import { User } from './user.model';
import { EventEmitter } from '@angular/core';

export class UserService {
  usersChange = new EventEmitter<User[]>();
  users: User[] = [
    new User('Aidai', 'aidai@hawaii.edu', 'true', 'editor'),
    new User( 'Aslam', 'aidai@hawaii.edu', 'false', 'editor'),
  ];

  getUsers() {
    return this.users.slice();
  }
  addUser(user: User) {
    this.users.push(user);
    this.usersChange.emit(this.users);
  }

}
