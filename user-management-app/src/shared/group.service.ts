import { Group } from './group.model';
import { EventEmitter } from '@angular/core';

export class GroupService {
  groupsChange = new EventEmitter<Group[]>();
  groups: Group[] = [
    new Group('Hiking'),
    new Group('Dancing'),
  ];

  getGroups() {
    return this.groups.slice();
  }
  addGroup(group: Group) {
    this.groups.push(group);
    this.groupsChange.emit(this.groups);
  }
}
